# [Drone CI Server](https://github.com/drone/drone) apt packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-drone-server.asc https://packaging.gitlab.io/drone-server/gpg.key
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/drone-server drone main" | sudo tee /etc/apt/sources.list.d/morph027-drone-server.list
```
